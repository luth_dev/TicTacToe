#include "updater.h"
#include "version.h"
#include "version_data.h"
#include <QDebug>
#include <QMessageBox>

Updater::Updater()
{
    InitLocalVersion();
    GetLatestVersion();
    i_VersionState = CompareVersion();
    ReportVersionState();
}

void Updater::GetLatestVersion()
{
    //Download latest version data now only used for testing

    remote_major = 0;
    remote_minor = 2;
    remote_patch = 0;
    remote_hash = "Test";

}

int Updater::CompareVersion()
{
    /*
     * Returnable values:
     * negative value - Something went wrong
     * 0 - Using latest version
     * 1 - Using older patch version
     * 2 - Using older minor version
     * 3 - Using older major version
     * 4 - Using newer version (most likely a tester , allow downgrade to older version in future)
     * Show a message box based on the return of this function (check value in Main.cpp)
     */
    if (local_patch == remote_patch && local_major == remote_major && local_minor == remote_minor)
        return 0;
    else if (local_major > remote_major)
        return 4;
    else if (local_major < remote_major)
        return 3;
    else if (local_minor < remote_minor)
        return 2;
    else if (local_minor > remote_minor)
        return 4;
    else if (local_patch < remote_patch)
        return 1;
    else if (local_patch > remote_patch)
        return 4;
    return -1;
}

void Updater::InitLocalVersion()
{
    local_build = Global::Version::GetBuild();
    local_major = Global::Version::GetMajor();
    local_minor = Global::Version::GetMinor();
    local_patch = Global::Version::GetPatch();
    local_hash = "Test";
}

void Updater::ReportVersionState()
{
    int sel;
    QMessageBox msgbox;
    msgbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    switch (i_VersionState)
    {
    case 0:
        //Do nothing
        break;
    case 1:
        msgbox.setText("You are using an older patch version! Do you want to update?");
        break;
    case 2:
        msgbox.setText("You are using an older minor version! Do you want to update?");
        break;
    case 3:
        msgbox.setText("You are using an older major version! Do you want to update?");
        break;
    case 4:
        msgbox.setText("You are using a newer version!");
        msgbox.setStandardButtons(QMessageBox::Ok);
        break;
    default:
        msgbox.setText("An error occured!");
        break;
    }
    if (i_VersionState)
        sel = msgbox.exec();
    else
        return;
    msgbox.close();
    switch (sel)
    {
    default:
    msgbox.setText("Not yet implemented :(");
    msgbox.setStandardButtons(QMessageBox::Ok);
    msgbox.exec();
        break;
    }

    // ToDo: Implement selection for buttons
}
