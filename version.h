#ifndef VERSION_H
#define VERSION_H

#include <QString>

namespace Global
{
    namespace Version
    {
    const int GetMajor();
    const int GetMinor();
    const int GetPatch();
    const int GetBuild();
    const QString GetVersionString();
    }
}

#endif // VERSION_H
