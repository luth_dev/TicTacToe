#include "version.h"
#include "version_data.h"
#include "build_data.h"

namespace Global
{
    namespace Version
    {
        const int GetBuild()
        {
            return _BUILD;
        }

        const int GetMajor()
        {
            return _MAJOR_VERSION;
        }

        const int GetMinor()
        {
            return _MINOR_VERSION;
        }

        const int GetPatch()
        {
            return _PATCH_VERSION;
        }

        const QString GetVersionString()
        {
            return QString(_VERSION_STR);
        }
    }
}
