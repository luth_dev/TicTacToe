#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include "updater.h"

namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = 0);
    ~MainMenu();

private slots:
    void on_btn_exit_clicked();

private:
    Ui::MainMenu *ui;
};

#endif // MAINMENU_H
