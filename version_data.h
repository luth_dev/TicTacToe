#ifndef VERSION_DATA_H
#define VERSION_DATA_H
 #define TO_STR(x) #x
 #define STR(x) TO_STR(x)
 #define _MAJOR_VERSION 0
 #define _MINOR_VERSION 1
 #define _PATCH_VERSION 0
 #define _VERSION_STR STR(_MAJOR_VERSION) "." STR(_MINOR_VERSION) "." STR(_PATCH_VERSION)
#endif
