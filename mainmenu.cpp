#include "mainmenu.h"
#include "ui_mainmenu.h"
#include <QMessageBox>
#include <QDesktopWidget>

MainMenu::MainMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    setFixedSize(700, 400);
    move((QApplication::desktop()->availableGeometry().width()-700)/2, (QApplication::desktop()->availableGeometry().height()-400)/2);
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::on_btn_exit_clicked()
{
    close();
}
