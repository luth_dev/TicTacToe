#include "mainmenu.h"
#include "updater.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Updater updater;
    MainMenu w;
    w.show();
    return a.exec();
}
