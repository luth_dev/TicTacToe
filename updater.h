#ifndef UPDATER_H
#define UPDATER_H

#include <QString>

class Updater
{
public:
    Updater();

    //Public functions

    int GetVersionState() { return i_VersionState; }
private:
    //Internal vars
    int i_VersionState = -1;

    //Local Update Data vars
    int local_major;
    int local_minor;
    int local_patch;
    int local_build;
    QString local_hash;

    //Remote Update Data vars
    int remote_major;
    int remote_minor;
    int remote_patch;
    int remote_build;
    QString remote_hash;

    //Internal functions

    void ReportVersionState();

    //Init functions
    int CompareVersion();
    void GetLatestVersion();
    void InitLocalVersion();
};

#endif // UPDATER_H
